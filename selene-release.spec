%define release_name Darkside
%define is_darkside 1

%define dist_version 1
%define rhel_dist_version 10

%if %{is_darkside}
%define bug_version darkside
%define releasever darkside
%define doc_version darkside
%else
%define bug_version %{dist_version}
%define releasever %{dist_version}
%define doc_version f%{dist_version}
%endif

%if 0%{?eln}
%bcond_with basic
%bcond_without eln
%bcond_with workstation
%else
%bcond_without basic
%bcond_with eln
%bcond_without workstation
%endif

%global dist %{?eln:.eln%{eln}}

# Changes should be submitted as pull requests under
#     https://src.seleneproject.org/rpms/selene-release

Summary:        Selene release files
Name:           selene-release
Version:        1
# The numbering is 0.<r> before a given Selene Linux release is released,
# with r starting at 1, and then just <r>, with r starting again at 1.
# Use '%%autorelease -p' before final, and then drop the '-p'.
Release:        %autorelease -p
License:        MIT
URL:            https://seleneproject.org/

Source1:        LICENSE
Source2:        Selene-Legal-README.txt

Source10:       85-display-manager.preset
Source11:       90-default.preset
Source12:       90-default-user.preset
Source13:       99-default-disable.preset
Source14:       80-server.preset
Source15:       80-workstation.preset
Source16:       org.gnome.shell.gschema.override
Source17:       org.projectatomic.rpmostree1.rules
Source18:       80-iot.preset
Source19:       distro-template.swidtag
Source20:       distro-edition-template.swidtag
Source21:       gnome-shell.conf
Source22:       80-coreos.preset
Source23:       zezere-ignition-url
Source24:       80-iot-user.preset

BuildArch:      noarch

Provides:       selene-release = %{version}-%{release}
Provides:       selene-release-variant = %{version}-%{release}

Provides:       system-release
Provides:       system-release(%{version})
Provides:       base-module(platform:f%{version})
Requires:       selene-release-common = %{version}-%{release}

# selene-release-common Requires: selene-release-identity, so at least one
# package must provide it. This Recommends: pulls in
# selene-release-identity-basic if nothing else is already doing so.
Recommends:     selene-release-identity-basic


BuildRequires:  redhat-rpm-config > 121-1
BuildRequires:  systemd-rpm-macros

%description
Selene release files such as various /etc/ files that define the release
and systemd preset files that determine which services are enabled by default.
# See https://docs.seleneproject.org/en-US/packaging-guidelines/DefaultServices/ for details.


%package common
Summary: Selene release files

Requires:   selene-release-variant = %{version}-%{release}
Suggests:   selene-release

Requires:   selene-repos(%{version})
Requires:   selene-release-identity = %{version}-%{release}

%if %{is_darkside}
# Make $releasever return "darkside" on Darkside
# https://pagure.io/releng/issue/7445
Provides:       system-release(releasever) = %{releasever}
%endif

# Fedora ships a generic-release package to make the creation of Remixes
# easier, but it cannot coexist with the selene-release[-*] packages, so we
# will explicitly conflict with it.
Conflicts:  generic-release

# rpm-ostree count me is now enabled in 90-default.preset
Obsoletes: fedora-release-ostree-counting < 35-0.32

%description common
Release files for Selene Linux


%if %{with basic}
%package identity-basic
Summary:        Package providing the basic Selene identity

RemovePathPostfixes: .basic
Provides:       selene-release-identity = %{version}-%{release}
Conflicts:      selene-release-identity


%description identity-basic
Provides the necessary files for a Selene installation that is not identifying
itself as a particular Edition or Spin.
%endif

%if %{with eln}
%package eln
Summary:        Base package for Selene ELN specific default configurations

RemovePathPostfixes: .eln
Provides:       selene-release = %{version}-%{release}
Provides:       selene-release-variant = %{version}-%{release}
Provides:       system-release
Provides:       system-release(%{version})
Provides:       base-module(platform:eln)
Requires:       selene-release-common = %{version}-%{release}
Provides:       system-release-product
Requires:       selene-repos-eln

Obsoletes:      redhat-release
Provides:       redhat-release

# selene-release-common Requires: selene-release-identity, so at least one
# package must provide it. This Recommends: pulls in
# selene-release-identity-eln if nothing else is already doing so.
Recommends:     selene-release-identity-eln


%description eln
Provides a base package for Selene ELN specific configuration files to
depend on.


%package identity-eln
Summary:        Package providing the identity for Selene ELN

RemovePathPostfixes: .eln
Provides:       selene-release-identity = %{version}-%{release}
Conflicts:      selene-release-identity


%description identity-eln
Provides the necessary files for a Selene installation that is identifying
itself as Selene ELN.
%endif

%if %{with workstation}
%package workstation
Summary:        Base package for Selene Workstation-specific default configurations

RemovePathPostfixes: .workstation
Provides:       selene-release = %{version}-%{release}
Provides:       selene-release-variant = %{version}-%{release}
Provides:       system-release
Provides:       system-release(%{version})
Provides:       base-module(platform:f%{version})
Requires:       selene-release-common = %{version}-%{release}
Provides:       system-release-product

# Third-party repositories, disabled by default unless the user opts in through selene-third-party
# Requires(meta) to avoid ordering loops - does not need to be installed before the release package
# Keep this in sync with silverblue above
Requires(meta):	selene-flathub-remote
Requires(meta):	selene-workstation-repositories

# selene-release-common Requires: selene-release-identity, so at least one
# package must provide it. This Recommends: pulls in
# selene-release-identity-workstation if nothing else is already doing so.
Recommends:     selene-release-identity-workstation


%description workstation
Provides a base package for Selene Workstation-specific configuration files to
depend on.


%package identity-workstation
Summary:        Package providing the identity for Selene Workstation Edition

RemovePathPostfixes: .workstation
Provides:       selene-release-identity = %{version}-%{release}
Conflicts:      selene-release-identity


%description identity-workstation
Provides the necessary files for a Selene installation that is identifying
itself as Selene Workstation Edition.
%endif


%prep
sed -i 's|@@VERSION@@|%{dist_version}|g' %{SOURCE2}

%build

%install
install -d %{buildroot}%{_prefix}/lib
echo "Selene release %{version} (%{release_name})" > %{buildroot}%{_prefix}/lib/selene-release
echo "cpe:/o:seleneproject:selene:%{version}" > %{buildroot}%{_prefix}/lib/system-release-cpe

# Symlink the -release files
install -d %{buildroot}%{_sysconfdir}
ln -s ../usr/lib/selene-release %{buildroot}%{_sysconfdir}/selene-release
ln -s ../usr/lib/system-release-cpe %{buildroot}%{_sysconfdir}/system-release-cpe
ln -s selene-release %{buildroot}%{_sysconfdir}/redhat-release
ln -s selene-release %{buildroot}%{_sysconfdir}/system-release

# Create the common os-release file
%{lua:
  function starts_with(str, start)
   return str:sub(1, #start) == start
  end
}
%define starts_with(str,prefix) (%{expand:%%{lua:print(starts_with(%1, %2) and "1" or "0")}})
%if %{starts_with "a%{release}" "a0"}
  %global prerelease \ Prerelease
%endif

cat << EOF >> os-release
NAME="Selene Linux"
VERSION="%{dist_version} (%{release_name}%{?prerelease})"
ID=selene
VERSION_ID=%{dist_version}
VERSION_CODENAME=""
PLATFORM_ID="platform:f%{dist_version}"
PRETTY_NAME="Selene Linux %{dist_version} (%{release_name}%{?prerelease})"
ANSI_COLOR="0;38;2;60;110;180"
LOGO=selene-logo-icon
CPE_NAME="cpe:/o:seleneproject:selene:%{dist_version}"
HOME_URL="https://seleneproject.org/"
DOCUMENTATION_URL="https://docs.seleneproject.org/en-US/selene/%{doc_version}/system-administrators-guide/"
SUPPORT_URL="https://ask.seleneproject.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Selene"
REDHAT_BUGZILLA_PRODUCT_VERSION=%{bug_version}
REDHAT_SUPPORT_PRODUCT="Selene"
REDHAT_SUPPORT_PRODUCT_VERSION=%{bug_version}
PRIVACY_POLICY_URL="https://seleneproject.org/wiki/Legal:PrivacyPolicy"
EOF

# Create the common /etc/issue
echo "\S" > %{buildroot}%{_prefix}/lib/issue
echo "Kernel \r on an \m (\l)" >> %{buildroot}%{_prefix}/lib/issue
echo >> %{buildroot}%{_prefix}/lib/issue
ln -s ../usr/lib/issue %{buildroot}%{_sysconfdir}/issue

# Create /etc/issue.net
echo "\S" > %{buildroot}%{_prefix}/lib/issue.net
echo "Kernel \r on an \m (\l)" >> %{buildroot}%{_prefix}/lib/issue.net
ln -s ../usr/lib/issue.net %{buildroot}%{_sysconfdir}/issue.net

# Create /etc/issue.d
mkdir -p %{buildroot}%{_sysconfdir}/issue.d

mkdir -p %{buildroot}%{_swidtagdir}

# Create os-release files for the different editions

%if %{with basic}
# Basic
cp -p os-release \
      %{buildroot}%{_prefix}/lib/os-release.basic
%endif


%if %{with eln}
# ELN
cp -p os-release \
      %{buildroot}%{_prefix}/lib/os-release.eln
echo "VARIANT=\"ELN\"" >> %{buildroot}%{_prefix}/lib/os-release.eln
echo "VARIANT_ID=eln" >> %{buildroot}%{_prefix}/lib/os-release.eln
sed -i -e 's|PLATFORM_ID=.*|PLATFORM_ID="platform:eln"|' %{buildroot}/%{_prefix}/lib/os-release.eln
sed -i -e 's|PRETTY_NAME=.*|PRETTY_NAME="Selene ELN"|' %{buildroot}/%{_prefix}/lib/os-release.eln
sed -i -e 's|DOCUMENTATION_URL=.*|DOCUMENTATION_URL="https://docs.seleneproject.org/en-US/eln/"|' %{buildroot}%{_prefix}/lib/os-release.eln
sed -e "s#\$version#%{bug_version}#g" -e 's/$edition/ELN/;s/<!--.*-->//;/^$/d' %{SOURCE20} > %{buildroot}%{_swidtagdir}/org.seleneproject.Selene-edition.swidtag.eln
%endif


%if %{with workstation}
# Workstation
cp -p os-release \
      %{buildroot}%{_prefix}/lib/os-release.workstation
echo "VARIANT=\"Workstation Edition\"" >> %{buildroot}%{_prefix}/lib/os-release.workstation
echo "VARIANT_ID=workstation" >> %{buildroot}%{_prefix}/lib/os-release.workstation
sed -i -e "s|(%{release_name}%{?prerelease})|(Workstation Edition%{?prerelease})|g" %{buildroot}%{_prefix}/lib/os-release.workstation
sed -e "s#\$version#%{bug_version}#g" -e 's/$edition/Workstation/;s/<!--.*-->//;/^$/d' %{SOURCE20} > %{buildroot}%{_swidtagdir}/org.seleneproject.Selene-edition.swidtag.workstation
# Add gnome-shell to dnf protected packages list for Workstation
install -Dm0644 %{SOURCE21} -t %{buildroot}%{_sysconfdir}/dnf/protected.d/

# Workstation
install -Dm0644 %{SOURCE15} -t %{buildroot}%{_prefix}/lib/systemd/system-preset/
# Override the list of enabled gnome-shell extensions for Workstation
install -Dm0644 %{SOURCE16} -t %{buildroot}%{_datadir}/glib-2.0/schemas/

%endif


# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release


# Set up the dist tag macros
install -d -m 755 %{buildroot}%{_rpmconfigdir}/macros.d
cat >> %{buildroot}%{_rpmconfigdir}/macros.d/macros.dist << EOF
# dist macros.

%%__bootstrap         ~bootstrap
%if 0%{?eln}
%%rhel              %{rhel_dist_version}
%%el%{rhel_dist_version}                1
# Although eln is set in koji tags, we put it in the macros.dist file for local and mock builds.
%%eln              %{eln}
%%dist                %%{!?distprefix0:%%{?distprefix}}%%{expand:%%{lua:for i=0,9999 do print("%%{?distprefix" .. i .."}") end}}.el%%{eln}%%{?with_bootstrap:%{__bootstrap}}
%else
%%selene              %{dist_version}
%%fc%{dist_version}                1
%%dist                %%{!?distprefix0:%%{?distprefix}}%%{expand:%%{lua:for i=0,9999 do print("%%{?distprefix" .. i .."}") end}}.fc%%{selene}%%{?with_bootstrap:%{__bootstrap}}
%endif
EOF

# Install licenses
mkdir -p licenses
install -pm 0644 %{SOURCE1} licenses/LICENSE
install -pm 0644 %{SOURCE2} licenses/Selene-Legal-README.txt

# Default system wide
install -Dm0644 %{SOURCE10} -t %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -Dm0644 %{SOURCE11} -t %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -Dm0644 %{SOURCE12} -t %{buildroot}%{_prefix}/lib/systemd/user-preset/
# The same file is installed in two places with identical contents
install -Dm0644 %{SOURCE13} -t %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -Dm0644 %{SOURCE13} -t %{buildroot}%{_prefix}/lib/systemd/user-preset/

# Create distro-level SWID tag file
install -d %{buildroot}%{_swidtagdir}
sed -e "s#\$version#%{bug_version}#g" -e 's/<!--.*-->//;/^$/d' %{SOURCE19} > %{buildroot}%{_swidtagdir}/org.seleneproject.Selene-%{bug_version}.swidtag
install -d %{buildroot}%{_sysconfdir}/swid/swidtags.d
ln -s %{_swidtagdir} %{buildroot}%{_sysconfdir}/swid/swidtags.d/seleneproject.org


%files common
%license licenses/LICENSE licenses/Selene-Legal-README.txt
%{_prefix}/lib/selene-release
%{_prefix}/lib/system-release-cpe
%{_sysconfdir}/os-release
%{_sysconfdir}/selene-release
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/system-release-cpe
%attr(0644,root,root) %{_prefix}/lib/issue
%config(noreplace) %{_sysconfdir}/issue
%attr(0644,root,root) %{_prefix}/lib/issue.net
%config(noreplace) %{_sysconfdir}/issue.net
%dir %{_sysconfdir}/issue.d
%attr(0644,root,root) %{_rpmconfigdir}/macros.d/macros.dist
%dir %{_prefix}/lib/systemd/user-preset/
%{_prefix}/lib/systemd/user-preset/90-default-user.preset
%{_prefix}/lib/systemd/user-preset/99-default-disable.preset
%dir %{_prefix}/lib/systemd/system-preset/
%{_prefix}/lib/systemd/system-preset/85-display-manager.preset
%{_prefix}/lib/systemd/system-preset/90-default.preset
%{_prefix}/lib/systemd/system-preset/99-default-disable.preset
%dir %{_swidtagdir}
%{_swidtagdir}/org.seleneproject.Selene-%{bug_version}.swidtag
%dir %{_sysconfdir}/swid
%{_sysconfdir}/swid/swidtags.d


%if %{with basic}
%files
%files identity-basic
%{_prefix}/lib/os-release.basic
%endif


%if %{with eln}
%files eln
%files identity-eln
%{_prefix}/lib/os-release.eln
%attr(0644,root,root) %{_swidtagdir}/org.seleneproject.Selene-edition.swidtag.eln
%endif




%if %{with workstation}
%files workstation
%files identity-workstation
%{_prefix}/lib/os-release.workstation
%attr(0644,root,root) %{_swidtagdir}/org.seleneproject.Selene-edition.swidtag.workstation
%{_sysconfdir}/dnf/protected.d/gnome-shell.conf
%{_datadir}/glib-2.0/schemas/org.gnome.shell.gschema.override
%{_prefix}/lib/systemd/system-preset/80-workstation.preset
%endif



%changelog
%autochangelog
